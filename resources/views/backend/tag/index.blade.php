@extends('backend.layouts.master')

@push('title','Tag')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush

@section('content')
    
<div class="animated fadeIn">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body card-header">
                    <h4 class="box-title">Tag </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        <form action="{{ route('tag.store')}}" method="post">
                                @csrf
                                    <div class="form-group has-success">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="cc-name" class="control-label">Add Tag</label>
                                                </div>
                                                <div class="col-md-4">
                                                <input id="cc-name" name="name" type="text" class="form-control" placeholder="Enter Tag Name">
                                                </div>
                                                <input type="submit" class="btn btn-primary">
                                            </div>
                                        </div>
                            </form>
                        
                                <div>
                        </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @php $i = 1; @endphp
                    <table id="example" class="table text-center table-bordered">
                        <thead>
                            <tr>
                               <th>Sl No</th>
                               <th>name</th>
                               <th>action</th>
                            </tr>
                        </thead>
                        <tbody>
                           @foreach ($data as $item)
                           <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->name }}</td>
                                <td><a class="btn btn-info mr-2" href="{{ route('tag.edit',$item->id) }}">Edit </a>
                                    <form class="d-inline" action="{{ route('tag.destroy',$item->id)}}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger" onclick="return confirm('Are You Sure?')"> Delete</button>
                                    </form>
                                </td>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

