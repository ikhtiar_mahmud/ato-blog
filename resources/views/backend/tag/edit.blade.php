@extends('backend.layouts.master')

@push('title','Tag')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush

@section('content')
    
<div class="animated fadeIn">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body card-header">
                    <h4 class="box-title">Tag </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                                @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        <form action="{{ route('tag.update',$data->id)}}" method="post">
                                @csrf
                                @method('put')
                                    <div class="form-group has-success">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label for="cc-name" class="control-label">Update Tag</label>
                                                </div>
                                                <div class="col-md-4">
                                                <input id="cc-name" name="name" type="text" class="form-control" placeholder="Enter Tag Name" value="{{ $data['name'] }}">
                                                </div>
                                                <input type="submit" class="btn btn-primary">
                                            </div>
                                        </div>
                            </form>
                        
                                <div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

