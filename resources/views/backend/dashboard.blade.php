@extends('backend.layouts.master')

@section('content')
    
<div class="animated fadeIn">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body card-header">
                    <h4 class="box-title">Dashboard </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            <div id="traffic-chart" class="traffic-chart"></div>
                        </div>
                    </div>
                </div>
                <div class="card-body"></div>
            </div>
        </div>
    </div>


</div>
@endsection

