 <!-- Left Panel -->
    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">
            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                    <a href="{{ route('dashboard')}}"><i class="menu-icon fa fa-laptop"></i>Dashboard </a>
                    </li>
                    <li>
                    <a href="{{ route('category.index') }}"><i class="menu-icon fa fa-folder-open"></i>Category </a>
                    </li>
                    <li>
                        <a href="{{ route('tag.index') }}"><i class="menu-icon fa  fa-tags"></i>Tag </a>
                    </li>
                    <li>
                        <a href="{{ route('post.index') }}"><i class="menu-icon fa fa-laptop"></i>Article </a>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
    <!-- /#left-panel -->