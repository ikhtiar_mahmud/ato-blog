@extends('backend.layouts.master')

@push('title','Post')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush

@section('content')
    
<div class="animated fadeIn">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body card-header">
                    <h4 class="box-title">Post </h4>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form action="{{ route('post.update',$post->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                    <div class="form-group has-success">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="title" class="mt-2">Title</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input id="title" name="title" type="text" class="form-control" placeholder="Enter Title" value='{{ $post->title }}'>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="cat" class="mt-2">Category</label>
                                            </div>
                                            <div class="col-md-4">
                                                <select class="form-control" name="cat_id" id="cat">
                                                    
                                                    @foreach ($categorys as $item)
                                                        <option {{ ( $item->id == $post->category->id) ? 'selected' : '' }} value="{{ $item->id}}">{{ $item->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="tag" class="mt-2">Tag</label>
                                            </div>
                                            <div class="col-md-4">
                                                @foreach ($tags as $item)
                                                    <input type="checkbox" name='tags[]' value="{{$item->id}}" class="mr-2">{{$item->name}}
                                                @endforeach
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="mt-2">Image</label>
                                            </div>
                                            <div class="col-md-2">
                                                <img src="{{ asset('/uploads/post/'.$post->image)}}" alt="" width="150" height="100">
                                            </div>
                                            <div class="col-md-4">
                                               <input type="file" name="image">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label for="text" class="mt-2">Body</label>
                                            </div>
                                            <div class="col-md-10">
                                               <textarea name="body" id="text">
                                                    {{ $post->body }}
                                                </textarea>
                                                <script> CKEDITOR.replace( 'text' );</script>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                            <div class="row">
                                                <div class="col-md-2 offset-md-2">
                                                    <input type="submit" value="Submit" class="btn btn-primary">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

