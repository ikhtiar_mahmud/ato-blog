@extends('backend.layouts.master')

@push('title','Post')

@push('css')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@endpush

@section('content')
    
<div class="animated fadeIn">

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body card-header">
                    <h4 class="box-title">Post </h4>
                </div>
                <div class="card-body">
                    <a class="btn btn-success mb-2" href="{{ route('post.create') }}">Add New</a>
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                    <table id="example" class="table text-center table-bordered">
                        <thead>
                            <tr>
                               <th>Sl No</th>
                               <th>Title</th>
                               <th>Body</th>
                               <th>Category</th>
                               <th>Image</th>
                               <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $item->title }}</td>
                                <td>{!! html_entity_decode(str_limit($item->body,'20')) !!}</td>
                                <td>{{ $item->category->name }}</td>
                                <td><img width="100" height="50" src="{{ asset('/uploads/post/'.$item->image)}}" alt=""></td>
                                <td><a class="btn btn-info mr-2" href="{{ route('post.edit',$item->id) }}">Edit </a>
                                    <form class="d-inline" action="{{ route('post.destroy',$item->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger" onclick="return confirm('Are You Sure?')"> Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $data->links()}}
                </div>
            </div>
        </div>
    </div>


</div>
@endsection

