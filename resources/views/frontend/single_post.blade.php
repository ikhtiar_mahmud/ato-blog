@extends('frontend.layouts.master')

@section('title',$item->title)

@section('content')
<div class="container">
        <div class="col-md-12  col-lg-12">
          <div class="post  post--fullsize">
            <div class="post__info  post__info--date">
              <span>{{ $item->created_at->diffForHumans() }}</span>
            </div>
            <div class="post__title">
              <h1><a href="single-post.html">Very Kind and Beautiful Cat</a></h1>
            </div>
            <div class="post__info  post__info--category">
              <span><a href="#">{{ $item->category->name}}</a></span>
            </div>
            <div class="post__image">
              <a href="single-post.html"><img src="{{ asset('uploads/post/'.$item->image)}}" alt="{{ $item->title }}""></a>
            </div>
            <div class="post__content">
                {!! html_entity_decode($item->body)!!}
            </div>
            <div class="post__tags">
              <h3>Tags</h3>
              <div class="post__tags-list">
                @foreach ($item->tags as $tag)
                  <a href="#">{{ $tag->name }}</a>
                @endforeach
                
              </div>
            </div>
            <div class="post__footer">
              {{-- <div class="post__info  post__info--author">
                <span>By <a href="#">Leopold</a></span>
                <span><a href="#">3 Comments</a></span>
              </div> --}}
              <div class="post__footer-social">
                <span>Share:</span>
                <div class="post__footer-social-icons">
                  <a href="#">
                    <svg>
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                    </svg>
                  </a>
                  <a href="#">
                    <svg>
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
                    </svg>
                  </a>
                  <a href="#">
                    <svg>
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
                    </svg>
                  </a>
                  <a href="#">
                    <svg>
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection