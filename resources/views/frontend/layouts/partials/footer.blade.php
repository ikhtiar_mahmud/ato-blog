<footer class="footer">
    <div class="container  footer__top">
      <div class="col-sm-5  col-md-5">
        <div class="footer__top-about">
          <h4>Leopold.</h4>
          <p>Suspendisse lobortis commodo ullamcorper. Duis pretium convallis odio non varius.</p>
          <p>Phone: +123.456.789</p>
          <p>Email: <a href="mailto:mail@leopold.com">mail@leopold.com</a></p>
        </div>
      </div>
      <div class="col-sm-3  col-md-3">
        <h4>Categories</h4>
        <nav class="footer__nav">
          <ul class="footer__nav-items">
            @php
                $category= DB::table('categories')->take(5)->get();
            @endphp
              @foreach ($category as $item)
            <li class="footer__nav-item">
                <a href="#">{{ $item->name }}</a>
            </li>
            @endforeach
          </ul>
        </nav>
      </div>
      <div class="col-sm-4  col-md-4">
        <h4>Recent Posts</h4>
        @php
            $populer = DB::table('posts')->inRandomOrder()->take(5)->get();
        @endphp
        @foreach ($populer as $item)
        <div class="footer__recent-post">
            <div class="footer__recent-post-info">
              <div class="footer__recent-post-content">
                <a href="{{ route('single_post',$item->id)}}">{{ $item->title }}</a>
              </div>
            </div>
          </div>
        @endforeach
       
      </div>
    </div>
    <div class="container  footer__bottom">
      <div class="col-sm-8  col-md-8  footer__bottom-copyright">
        <p>Powered By Angel Thought Organization</p>
      </div>
      <div class="col-sm-4  col-md-4  footer__bottom-social">
        <a href="#">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
          </svg>
        </a>
        <a href="#">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
          </svg>
        </a>
        <a href="#">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
          </svg>
        </a>
        <a href="#">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
          </svg>
        </a>
        <a href="#">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
          </svg>
        </a>
      </div>
    </div>
  </footer>