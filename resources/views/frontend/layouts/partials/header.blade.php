<header class="header">
    <nav class="nav">
      <div class="nav__container  container">
        <ul class="nav__list">
          <li class="nav__item  nav__item--active">
            <a href="{{ route('home')}}">Home</a>
          </li>
          <li class="nav__item">
            <a href="#">About</a>
          </li>
          <li class="nav__item">
            <a href="#">Contact</a>
          </li>
        </ul>
        <div class="nav__search  search">
          <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-search"></use>
          </svg>
        </div>
      </div>
    </nav>
    <div class="col-md-12  nav-toggle">
      <svg class="nav-toggle__icon">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-toggle"></use>
      </svg>
    </div>
    <div class="col-md-12  header__logo">
      <div class="logo">
      <h1><a class="logo__link" href="{{ route('home')}}">CrazY Coder Blog</a></h1>
        <div class="logo__description">Personal Blog Template</div>
      </div>
    </div>
  </header>