@extends('frontend.layouts.master')

@section('title','Home')

@section('content')
<div class="container">
    <div class="col-md-8  col-lg-9">
      @foreach ($data as $item)
      <div class="post">
          <div class="post__info  post__info--date">
            <span>{{ $item->created_at->diffForHumans() }}</span>
          </div>
          <div class="post__title">
            <h2><a href="{{ route('single_post',$item->id)}}">{{ $item->title}}</a></h2>
          </div>
          <div class="post__info  post__info--category">
            <span><a href="#">{{ $item->category->name}}</a></span>
          </div>
          <div class="post__image">
            <a href="{{ route('single_post',$item->id)}}"><img src="{{ asset('uploads/post/'.$item->image)}}" alt="{{ $item->title }}"></a>
          </div>
          <div class="post__content">
            <p>{!! html_entity_decode(str_limit($item->body,'250'))!!}</p>
          </div>
          <div class="post__content-more-link">
            <a href="{{ route('single_post',$item->id)}}">Read more</a>
          </div>
          <div class="post__footer">
            {{-- <div class="post__info  post__info--author">
              <span>By <a href="#">Leopold</a></span>
              <span><a href="#">3 Comments</a></span>
            </div> --}}
            <div class="post__footer-social">
              <span>Share:</span>
              <div class="post__footer-social-icons">
                <a href="#">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                  </svg>
                </a>
                <a href="#">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
                  </svg>
                </a>
                <a href="#">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
                  </svg>
                </a>
                <a href="#">
                  <svg>
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </div>
      @endforeach
      <nav class="blog-pagination">
        {{-- <ul class="blog-pagination__items">
          <li class="blog-pagination__item  blog-pagination__item--active">
            <a>1</a>
          </li>
          <li class="blog-pagination__item">
            <a href="#">2</a>
          </li>
          <li class="blog-pagination__item">
            <a href="#">Next Page</a>
          </li>
        </ul> --}}
        {{ $data->links()}}
      </nav>
    </div>
    <div class="col-md-4  col-lg-3">
      <div class="sidebar-widget">
        <h3>About Me</h3>
        <div class="sidebar-widget__about-me">
          <div class="sidebar-widget__about-me-image">
            <img src="{{ asset('ui/frontend/img/about-me.jpg')}}" alt="About Me">
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse lobortis commodo ullamcorper.</p>
        </div>
      </div>
      <div class="sidebar-widget">
        <h3>Follow Me</h3>
        <div class="sidebar-widget__follow-me">
          <div class="sidebar-widget__follow-me-icons">
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-twitter"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-google"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
              </svg>
            </a>
            <a href="#">
              <svg>
                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
              </svg>
            </a>
          </div>
        </div>
      </div>
      <div class="sidebar-widget">
        <h3>Popular Posts</h3>
        <div class="sidebar-widget__popular">
          @foreach ($populer as $item)
          <div class="sidebar-widget__popular-item">
              <div class="sidebar-widget__popular-item-image">
              <a href="{{ route('single_post',$item->id)}}"><img src="{{ asset('uploads/post/'.$item->image)}}" alt="{{ $item->title }}"></a>
              </div>
              <div class="sidebar-widget__popular-item-info">
                <div class="sidebar-widget__popular-item-content">
                  <a href="{{ route('single_post',$item->id)}}">{{ $item->title }}</a>
                </div>
                <div class="sidebar-widget__popular-item-date">
                  <span>{{ $item->created_at->diffForHumans() }}</span>
                </div>
              </div>
            </div>
          @endforeach
          
        </div>
      </div>
      <div class="sidebar-widget">
        <h3>Tag Cloud</h3>
        <div class="sidebar-widget__tag-cloud">
          @foreach ($tag as $item)
        <a href="#">{{ $item->name}}</a>
          @endforeach
          
        </div>
      </div>
      <div class="sidebar-widget">
        <h3>Subscribe</h3>
        <div class="sidebar-widget__subscribe">
          <p>Follow my latest news</p>
          <form action="index.html">
            <input type="text" placeholder="Your email">
            <input class="sidebar-widget__subscribe-submit" type="submit" value="Submit">
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection