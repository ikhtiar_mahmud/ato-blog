<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontend.index');
// });

Route::get('/','HomeController@index')->name('home');
Route::get('/single-post/{id}','HomeController@singlePost')->name('single_post');

Route::group(['middleware' => 'auth'], function(){
    
    Route::get('/admin','DashboardController@index')->name('dashboard');
    Route::resource('/tag','TagController');
    Route::resource('/category','CategoryController');
    Route::resource('/post','PostController');
});




Auth::routes();    
