<?php

namespace App\Http\Controllers;
use App\Tag;

use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index(){
        $data = Tag::orderBy('id','DESC')->get();
        return view('backend.tag.index',compact('data'));
    }

    public function store(Request $request){
        $data = $request->all();

        Tag::create([
            'name' => $data['name'],
        ]);
        
        
        return redirect()->back()->with('status','Tag Successfully Created!');
    }

    public function edit($id){
        $data = Tag::findorfail($id);
        return view('backend.tag.edit',compact('data'));
    }

    public function update(Request $request,$id){
        $data = Tag::findorfail($id);
        
        $data->name = $request->name;
        $data->save();

        return redirect()->route('tag.index')->with('status','Tag Updated Successfully!');
    }

    public function destroy($id){
        $data = Tag::findorfail($id);
        $data->delete();

        return redirect()->route('tag.index')->with('status','Tag Deleted!');
    }
}
