<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use App\Category;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{

    const UPLOAD_DIR = '/uploads/post/';

    public function index(){
        $paginatePerPage = 10;
            $pagenumber = request('page');
            if ($pagenumber > 1) {
                $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
            } else {
                $serial = 0;
            }
        $data = Post::paginate($paginatePerPage);
        return view('backend.post.index',compact('data','serial'));
    }

    public function create(){
        $categorys = Category::all();
        $tags      = Tag::all();

        $data = [
            'categorys' => $categorys,
            'tags'      => $tags,
        ];
        return view('backend.post.create',compact('data'));
    }

    public function store(Request $request){
        $data = $request->all();
    
       if($request->hasFile('image')){
             $data['image'] = $this->uploadImage($request->image);
       }

    
      $data = Post::create([
           'title'  => $data['title'],
           'cat_id' => $data['cat_id'],
           'image'  => $data['image'],
           'body'   => $data['body'],
       ]);

       $data->tags()->attach($request->tags);
       
       return redirect()->route('post.index')->with('status','Post Created Successfully!');
    }

    public function edit($id){
        $post = Post::findorfail($id);
        $categorys = Category::all();
        $tags      = Tag::all();

        return view('backend.post.edit',compact('post','categorys','tags'));
    }

    public function update(Request $request,$id){
        $data = $request->all();
        $post = Post::findorfail($id);

        if($request->hasFile('image')){
            $image_path = public_path() . '/uploads/post/' . $post->image;
            unlink($image_path);
            $data['image'] = $this->uploadImage($request->image);
       }else{
           $data['image']  = $post->image;
       }

        $post->title  = $request->title;
        $post->cat_id = $request->cat_id;
        $post->image  = $data['image'];
        $post->body   = $request->body;

        $post->save();
        $post->tags()->sync($request->tags);

        return redirect()->route('post.index')->with('status','Post Updated Successfully!');
    }

    public function destroy($id){
        $post = Post::findorfail($id);
        $image_path = public_path() . '/uploads/post/' . $post->image;
        unlink($image_path);
        $post->delete();

        $post->tags()->detach();

        return redirect()->route('post.index')->with('status','Post Deleted Successfully!');
    }

    private function uploadImage($file) //$name
    {
        $extension = $file->getClientOriginalExtension($file);
        $fileName = date('y-m-d') . '_' . time() . '.' . $extension; //'_' . $name .
        Image::make($file)->resize(1170, 760)->save(public_path() . self::UPLOAD_DIR . $fileName);
        return $fileName;
    }  
}
