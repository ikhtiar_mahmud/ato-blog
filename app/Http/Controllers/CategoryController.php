<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $data = Category::orderBy('id','DESC')->get();
        return view('backend.category.index',compact('data'));
    }

    public function store(Request $request){
        $data = $request->all();

        Category::create([
            'name' => $data['name'],
        ]);
        
        
        return redirect()->back()->with('status','Category Successfully Created!');
    }

    public function edit($id){
        $data = Category::findorfail($id);
        return view('backend.category.edit',compact('data'));
    }

    public function update(Request $request,$id){
        $data = Category::findorfail($id);
        
        $data->name = $request->name;
        $data->save();

        return redirect()->route('category.index')->with('status','Category Updated Successfully!');
    }

    public function destroy($id){
        $data = Category::findorfail($id);
        $data->delete();

        return redirect()->route('category.index')->with('status','Category Deleted!');
    }
}
