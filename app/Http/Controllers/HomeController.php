<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $paginatePerPage = 10;
            $pagenumber = request('page');
            if ($pagenumber > 1) {
                $serial = ($pagenumber * $paginatePerPage) - $paginatePerPage;
            } else {
                $serial = 0;
            }
        $data    = Post::orderBy('id','DESC')->paginate($paginatePerPage);
        $populer = Post::inRandomOrder()->take(5)->get();
        $tag     = Tag::all();
        return view('frontend.index',compact('data','serial','tag','populer'));
    }

    public function singlePost($id){
        $item = Post::findorfail($id);

        return view('frontend.single_post',compact('item'));
    }
}
