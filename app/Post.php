<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $guarded = [];

    public function category(){
        return $this->belongsTo(Category::class,'cat_id','id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class)->withTimestamps();
    }
}
